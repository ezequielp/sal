<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Poll lib model
 *
 * @license		http://opensource.org/licenses/gpl-license.php GNU Public License
 * @author		WookieMonster
 * @link		http://github.com/wookiemonster
 */
use \Illuminate\Database\Eloquent\Model as Eloquent;

class Supracategoria_model extends Eloquent {
    protected $table = "supracategoria";

	/**
	 * Constructor
	 * 
	 * @access	public
	 * @return	null
	 */
	public function __construct()
	{

	}
	 
	public function categorias(){
		return $this->hasMany('Categoria_model','supracategoria_id');
	}
      
     
}
