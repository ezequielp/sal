<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Poll lib model
 *
 * @license		http://opensource.org/licenses/gpl-license.php GNU Public License
 * @author		WookieMonster
 * @link		http://github.com/wookiemonster
 */
use \Illuminate\Database\Eloquent\Model as Eloquent;

class User_model extends Eloquent {
    protected $table = "user";

	/**
	 * Constructor
	 * 
	 * @access	public
	 * @return	null
	 */
	public function __construct()
	{

	}

	public function colegios(){
		return $this->belongsToMany('Colegio_model','user_colegio','user_id','colegio_id');
	}
	
	public function roles(){
		return $this->belongsToMany('Rol_model','user_rol','user_id','rol_id');
	}
        
      
     
}
