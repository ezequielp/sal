<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Poll lib model
 *
 * @license		http://opensource.org/licenses/gpl-license.php GNU Public License
 * @author		WookieMonster
 * @link		http://github.com/wookiemonster
 */
class Base_model extends CI_Model {
	

	
	/**
	 * Constructor
	 * 
	 * @access	public
	 * @return	null
	 */
	public function __construct()
	{

	}

        public function validarCuit( $cuit ){
            $cuit = preg_replace( '/[^\d]/', '', (string) $cuit );
            if( strlen( $cuit ) != 11 ){
                    return false;
            }
            $acumulado = 0;
            $digitos = str_split( $cuit );
            $digito = array_pop( $digitos );

            for( $i = 0; $i < count( $digitos ); $i++ ){
                    $acumulado += $digitos[ 9 - $i ] * ( 2 + ( $i % 6 ) );
            }
            $verif = 11 - ( $acumulado % 11 );
            $verif = $verif == 11? 0 : $verif;

            return $digito == $verif;
        }

        // Funcion que chequea si un item con columna user_id le pertenece
        function isOwner($tabla = null,$id = null){
            $tabla_id = $tabla."_id";
            $user = $this->get_user_id();
            $result = $this->db->query("SELECT * FROM $tabla WHERE $tabla_id = ? AND user_id = ?",array($id,$user));
            if($result->num_rows() > 0){
                return true;
            }
            else{
                return false;
            }
        }

        function login($username = null,$password = null){
        //Chequear en la DB si existe este usuario y esta password
        if(!($username && $password)){
            return false;
        }
        $password = sha1($password);
	    $query = $this->db->query("SELECT * FROM user WHERE username = ? AND password = ?",array($username,$password));

            if(!($query->num_rows() > 0)){
             //No hay usuarios que matcheen!
             return false;
            }
            else{
                //Obtenemos la fecha de acceso
                $last_login = time();
                $data = array(
                    "last_login" => $last_login
                );
		        $user = $query->row();
                    $this->db->where('id',$user->id);
                    $this->db->update("user", $data);
                //Guadamos el ID en una variable de session
                $this->session->set_userdata("user_id",$user->id);
                return true;
            }
        }
        
        public function update_user(){
            
            $user_id = $this->input->post('id');
            $query = $this->db->query("SELECT * from user WHERE user_id = ?",array($user_id));
            
            $data = $this->input->post();

            if($query->num_rows > 0){
                        if($this->input->post('password') != ""){
                            $this->db->where('id', $this->input->post('id'));
                            $data['password'] = sha1($this->input->post('password'));
                            $this->db->update('user', Array("username" => $data['username'] , "password" => $data['password']));
                        }
                        else{
                            $this->db->where('id', $this->input->post('id'));
                            $this->db->update('user', Array("username" => $data['username']));
                        }
            }
            else{
                $this->db->insert('user', $this->input->post());
            }
        }
        
        function logout() {
            $this->session->unset_userdata("user_id");
            return true;
        }

        function is_logged(){
            $user_id = $this->session->userdata("user_id");
            if($user_id){
                return $user_id;
            }
            else{
                return false;
            }
        }
        
        function get_user_id(){
            return  $this->session->userdata("user_id");
        }
        function get_user(){
            $user_id = $this->session->userdata("user_id");

            $data = array(
            "user_id" => $user_id,
            );

            $query = $this->db->get_where("user", $data);

            return $query->row();
        }
        
        public function puede($permiso = null){
            $user_id = $this->get_user_id();
            $query = $this->db->query("SELECT slug from user "
                    . " LEFT JOIN user_rol ON user.id = user_rol.id"
                    . " LEFT JOIN permiso_rol ON user_rol.rol_id = permiso_rol.rol_id"
                    . " LEFT JOIN permiso ON permiso_rol.permiso_id = permiso.id"
                    . " WHERE user.id = ?",array($user_id));
            
            $permisos = $query->result_array();
            
            foreach ($permisos as $permisoP) {
                $lista[] = $permisoP['slug'];
            }
            
            if(!in_array($permiso, $lista)){
                redirect('overview');
            }
            
        }
        public function puede_ver($permiso = null){
            $user_id = $this->get_user_id();
            
            if(!$user_id){
                return 0;
            }
            $query = $this->db->query("SELECT slug from user "
                    . " LEFT JOIN user_rol ON user.id = user_rol.id"
                    . " LEFT JOIN permiso_rol ON user_rol.rol_id = permiso_rol.rol_id"
                    . " LEFT JOIN permiso ON permiso_rol.permiso_id = permiso.id"
                    . " WHERE user.id = ? AND slug = ?",array($user_id,$permiso));
 
            return $query->num_rows();
        }
        
        function user_forgot_generate($email){
             $user_email = $email;
             $code = substr( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ,mt_rand( 0 ,51 ) ,1 ) .substr( md5( time() ), 1);
             $data = array("code" => $code,
                 "user_email" => $user_email,
                 "fecha" => date("Y-m-d H:i:s"));
             
             $user = $this->db->query('SELECT * FROM user WHERE username = ?',array($user_email));
             if($user->num_rows() > 0){
                 $code = $this->db->query('SELECT * FROM user_forgot WHERE user_email = ?',array($user_email));
                 if($code->num_rows() > 0){
                     $this->db->where("user_email",$user_email);
                     $this->db->update('user_forgot',$data);
                 }
                 else{
                     $this->db->insert('user_forgot',$data);
                 }
                 return $data['code'];
             }
             else{
                 return 0;
             }

        }
            
        function register($username = null, $password = null) {
            //El username es unico
            if($this->can_register($username)){
                $data = array(
                    "username" => $username,
                    "password" => sha1($password),
                );
                $this->db->insert("user", $data);
                return $this->db->insert_id();
            }
            return false;
        }

        function can_register($username){
            $query = $this->db->query("SELECT * FROM user WHERE username = ?",array($username));
            return ($query->num_rows() < 1) ? true : false;
        }
        
        /*
         * user_rol: Tabla que tiene asociacion de rol_id y user_id
         * rol: Tabla que tiene rol_id y nombre
         */
        
        
        public function have_rol($nombre_rol = null, $user = null){
            if($user == null){
                $user = $this->session->userdata("user_id");
            }

            $rol = $this->db->get_where("rol", array("nombre" => $nombre_rol))->row()->rol_id;

            $roles = $this->get_roles($user);

            return in_array($rol,$roles);
        }
        public function get_roles($user = null){
            if($user == null){
                $user = $this->session->userdata("user_id");
            }
            $query = $this->db->get_where("user_rol", array("user_id" => $user));

            $roles = $query->result_array();
            $result = array();

            foreach ($roles as $rol) {
                $result[] = $rol['id'];
            }

            return $result;
        }
        public function fact($evento = null, $type = 0,$user_id = null){
            if(!$user_id){
                $user_id = $this->session->userdata("user_id");
            }    
                $data = array(
                    "user_id" => $user_id,
                    "nombre" => $evento,
                    "fecha" => date('Y-m-d H:i:s'),
                    "type" => $type,
                );
                $this->db->insert("fact", $data);
        }
        
        public function type_fact($type = 0){
            if($type == 0){
                return '<i class="fa fa-warning"></i>';
            }
            if($type == 1){
                return '<i class="fa fa-check"></i>';
                
            }
        }
        public function checkAccess(){
            if(!$this->Base_model->is_logged()){
                    $this->session->set_flashdata('msg', 'Acceso denegado');
                    redirect('login');
            }
        }
        public function getUsername($user_id = null){
            if(!$user_id){
                $user_id = $user = $this->session->userdata("user_id");
            }
            $name = $this->db->get_where("user", array("user_id" => $user_id));
            $name = $name->row_array();
            return $name['username'];
        }
        
  
        
        public function delete_by($tabla, $id){
            $this->db->where($tabla."_id",$id);
            $this->db->delete($tabla);
        }
        
        
        public function getCampaña($user_id = null,$campaña_id = null){
            if($campaña_id){
                $campañas = $this->db->query("SELECT * FROM campaña WHERE campaña_id = ? AND user_id = ?",array($campaña_id,$user_id));
                return $campañas->result(); 
            }
            else{
                $campañas = $this->db->query("SELECT * FROM campaña WHERE user_id = ?",array($user_id));
                return $campañas->result(); 
            }
        }
        
        public function getCampañaOne($campaña_id = null){
                $campaña = $this->db->query("SELECT * FROM campaña WHERE campaña_id = ?",array($campaña_id));
                return $campaña->row(); 
        }
        
        public function getVisitas($campaña_id = null){
            $visitas = $this->db->query("SELECT * FROM visita"
                 //   . " JOIN email ON email.email = visita.email"
                    . " WHERE campaña_id = ?"
                    . " ORDER BY visita_id ASC",array($campaña_id));
            return $visitas->result(); 
        }
        public function getVisitasMail($campaña_id = null){
            $visitas = $this->db->query("SELECT * FROM visita WHERE campaña_id = ? AND email <> ''  ",array($campaña_id));
            return $visitas->result(); 
        }
        
        
        public function getLocation(){
            $ip = $this->input->ip_address();
            $url = "http://api.ipinfodb.com/v3/ip-city/?key=d0f3675b7157d12df40822e8d5db96c79b51d310e3b998e4e2bbd2c7a6ba444e&ip=$ip&format=json";

            $d = file_get_contents($url);
            $data = json_decode($d , true);

       
            $info = array();
            if(strlen($data['countryCode']))
            {
            $info = array(
                'ip' => $data['ipAddress'] ,
                'country_code' => $data['countryCode'] ,
                'country_name' => $data['countryName'] ,
                'region_name' => $data['regionName'] ,
                'city' => $data['cityName'] ,
                'zip_code' => $data['zipCode'] ,
                'latitude' => $data['latitude'] ,
                'longitude' => $data['longitude'] ,
                'time_zone' => $data['timeZone'] ,
            );
            }
            return $info;
        }
        
        public function get_url($upload_id = null){
            $imagen = $this->db->query("SELECT file FROM uploads WHERE uploads_id = ?",array($upload_id));
            if($imagen->num_rows >0){
                $imagen = $imagen->row();
                $imagen = $imagen->file;
            }
            else{
                return 0;
            }
            return base_url().'uploads/media/'.$imagen;
        }
        
        public function statusPagado($campaña_id = null){
            
            $pagado = $this->db->query("SELECT * FROM pago"
                    . " LEFT JOIN plan ON plan.plan_id = pago.plan_id"
                    . " WHERE pago.campaña_id = ? "
                    . " AND ADDDATE(pago.fecha,plan.duracion) > CURDATE()"
                    . " ORDER BY pago.fecha DESC",array($campaña_id));
            
            if($pagado->num_rows() > 0){
                $pagado = $pagado->row();
                return $pagado->status;
            }
            else
                return 0;
            
            
        }
        
        public function getTotalSize($user_id = null){
            $pagado = $this->db->query("SELECT sum(size) as 'total' from uploads WHERE user_id = ?",array($user_id));
            $pagado = $pagado->row();
            return $pagado->total;
        }
        
        public function getTimePagado($campaña_id = null){
            $pagado = $this->db->query("SELECT pago.fecha, duracion FROM pago"
                    . " LEFT JOIN plan ON plan.plan_id = pago.plan_id"
                    . " WHERE pago.campaña_id = ? "
                    . " AND ADDDATE(pago.fecha,plan.duracion) > CURDATE()"
                    . " ORDER BY pago.fecha DESC",array($campaña_id));
            
            if($pagado->num_rows() > 0){
                $pagado = $pagado->row();
                return $pagado;
            }
            else
                return 0;
             
        }
        
         public function sendMail($mail = null,$mensaje = null,$titulo = null,$asunto = null,$mensaje_id = null){
                    if($mail){
                            $this->load->library('email');
                            $config['protocol']    = 'smtp';
                            $config['smtp_host']    = 'ssl://mail.consiliaweb.com';
                            $config['smtp_port']    = '465';
                            $config['smtp_timeout'] = '7';
                            $config['smtp_user']    = 'info@consiliaweb.com';
                            $config['smtp_pass']    = 'viena2837';
                            $config['charset']    = 'utf-8';
                            $config['newline']    = "\r\n";
                            $config['mailtype'] = 'html'; // or html
                            $config['validation'] = TRUE; // bool whether to validate email or not      

                            $this->db->insert('email',array('email' => $mail, 'visto' => 0, 'mensaje_id' => $mensaje_id));

                            $last_id=$this->db->insert_id();


                            
                            $this->email->initialize($config);
                            $this->email->set_newline("\r\n");
                            $this->email->from('info@consiliaweb.com', 'Consilia');
                            $this->email->to($mail);
                            $this->email->subject($asunto);

                            $data["mail"] = $last_id;
                            $data["titulo"] = $titulo;
                            $data["mensaje"] = $mensaje;

                            $body = $this->load->view('email.php',$data,TRUE);
                            $this->email->message($body);   
                            $this->email->send();
                    }
                } 
          
	
}
