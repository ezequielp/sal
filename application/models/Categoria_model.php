<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Poll lib model
 *
 * @license		http://opensource.org/licenses/gpl-license.php GNU Public License
 * @author		WookieMonster
 * @link		http://github.com/wookiemonster
 */
use \Illuminate\Database\Eloquent\Model as Eloquent;

class Categoria_model extends Eloquent {
    protected $table = "categoria";

	/**
	 * Constructor
	 * 
	 * @access	public
	 * @return	null
	 */
	public function __construct()
	{

	}
	 
	public function documentos(){
		return $this->hasMany('Documento_model','categoria_id');
	}
      
     
}
