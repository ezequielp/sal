<div class="gray-bg" style="background-color: #fff;" id="login-page">
    <div class="middle-box text-center loginscreen animated fadeInDown">
    <img src="https://www.egeasociacion.com.ar/wp-content/uploads/2019/07/EGE-signo-de-identidad-HOriz-mayus_1000-Provisorio-1-e1562354159633-1.jpg">
        <div>
            <div>
            </div>
            <h2 style="font-size: 18px;">Repositorio digital EGE</h2>
            <hr>
            <p>Ingrese con su usuario y contraseña</p>
            <script>
            <?php if($this->session->flashdata('msg')){ ?>
                <div class="alert alert-danger"> <?php echo $this->session->flashdata('msg'); ?> </div>
            <?php } ?>
            });
            </script>
            <form action="<?php echo base_url(); ?>login/ingresar" class="m-t" role="form" method="POST">
                <div class="form-group">
                    <input name="username" type="email" class="form-control" placeholder="micorreo@correo.com" required="">
                </div>
                <div class="form-group">
                    <input name="password" type="password" class="form-control" placeholder="Contraseña" required="">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Ingresar</button>

                <a href="#"><small>Olvidé la contraseña</small></a>
                <br>
                <br>
                Para acceder como usuario libre ingresá con:
                <br>
                Usuario: <strong style="font-size: 20px;font-weight:bold; color: darkorange;">usuario@ege</strong><br>
                Contraseña: <strong style="font-size: 20px;font-weight:bold; color: darkorange;">usuario</strong>
            </form>
            <br>
            <br>
            <br>
            <p class="m-t"> g-lion <small>V1.0 2018</small> </p>
        </div>
    </div>

</div>