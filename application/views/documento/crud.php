<div class="gray-bg" id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <h1 class="titulo"><?php echo $titulo ?></h1>
            <hr>
        </div>




        <?php if(in_array("Administrador",$user->roles()->pluck('nombre')->toArray()) ): ?>

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content tabla-elementos">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                <!-- crud -->
                                        <?php $css_files = $table->css_files; ?>
                                        <?php $js_files = $table->js_files; ?>
                                        <?php $js_lib_files = $table->js_lib_files; ?>
                                        <?php $js_config_files = $table->js_config_files; ?>
                                        <?php $output = $table->output; ?>
                                        <!-- Para el CRUD -->
                                        <?php foreach($css_files as $file): ?>
                                       <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
                                        <?php endforeach; ?>

                                        <?php $flag=true;?>

                                        <?php foreach($js_files as $file): ?>
                                                <script src="<?php echo $file; ?>"></script>
                                        <?php endforeach; ?>


                                        <!-- Para el CRUD -->
                                        <?php echo $output?>
                                <!-- crud -->
                            <?php if(isset($extra)): ?>
                                <?php echo $extra; ?>
                            <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
        </div>  
        <?php endif; ?>


        <div class="row">
                <div class="col-lg-3">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <div class="file-manager">
                            <hr>
                                <h5>Mostrar: </h5>
                                <ul class="folder-list" style="padding: 0">
                                <?php foreach($supracategorias as $supracategoria): ?>
                                    <li><a class="folder-supracategoria" <?php echo isset($supracategoria->color)? 'style="color:'.$supracategoria->color.';"' : '';  ?>  data-supracategoria="<?php echo $supracategoria->id; ?>" ><i <?php echo isset($supracategoria->color)? 'style="color:'.$supracategoria->color.';"' : '';  ?> class="fa fa-folder"></i> <?php echo $supracategoria->nombre; ?></a>
                                        <ul class="folder-list" style="padding-left: 20px;">
                                    <?php foreach($supracategoria->categorias as $categoria): ?>
                                            <li><a class="folder-categoria" <?php echo isset($supracategoria->color)? 'style="color:'.$supracategoria->color.';"' : '';  ?> data-categoria="<?php echo $categoria->id; ?>" ><i <?php echo isset($supracategoria->color)? 'style="color:'.$supracategoria->color.';"' : '';  ?> class="fa fa-folder"></i> <?php echo $categoria->nombre; ?></a></li>
                                    <?php endforeach; ?>
                                        </ul>
                                    </li>
                            
                                <?php endforeach; ?>
                                </ul>

                                <h5 class="tag-title">Tags</h5>
                                <ul class="tag-list" style="padding: 0">
                                    <?php foreach($tags as $tag): ?>
                                        <li><a href="" data-tag="<?php echo $tag; ?>"><?php echo $tag; ?></a></li>
                                    <?php endforeach; ?>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 animated fadeInRight">
                    <div class="row">
                        <div class="col-lg-12">
            <?php foreach($supracategorias as $supracategoria): ?>
                <?php foreach($supracategoria->categorias as $categoria): ?>
                    <?php $documentos = $categoria->documentos; ?>
                        <?php foreach($documentos as $documento): ?>
                            <div class="file-box" data-tag="<?php echo $documento->tag; ?>" data-categoria="<?php echo $categoria->id; ?>" data-supracategoria="<?php echo $supracategoria->id; ?>" >
                                <div class="file">
                                    <a target="_blank" href="<?php echo base_url(); ?>assets/uploads/files/documentos/<?php echo $documento->nombre; ?>">
                                        <span class="corner"></span>
                                        <?php if(!isset($documento->imagen)): ?>
                                        <div class="icon">
                                            <i <?php echo isset($supracategoria->color)? 'style="color:'.$supracategoria->color.';"' : '';  ?> class="<?php echo $documento->icono; ?>"></i>
                                        </div>
                                        <?php else: ?>
                                        <div class="icon">
                                            <img width="100%" src="<?php echo base_url(); ?>assets/uploads/files/<?php echo $documento->imagen; ?>">
                                        </div>
                                        <?php endif; ?>
                                        <div class="file-name">
                                                    <?php echo $documento->descripcion; ?>
                                                    <br/>
                                                    <small><?php 
                                                    setlocale(LC_TIME, 'es_ES.UTF-8'); \Carbon\Carbon::setLocale('es');
                                                    echo \Carbon\Carbon::createFromTimeStamp(strtotime($documento->created_at))->diffForHumans() ?></small>
                                        </div>
                                    </a>
                                </div>
                            </div>                        
                        <?php endforeach; ?>   
                <?php endforeach; ?>
            <?php endforeach; ?>
                            

    </div>
</div>
</div>
</div>
</div>

        


    </div>
</div>
<style>
.folder-categoria{
    transition: all ease 0.3s;
}
.folder-categoria.active{
    color: #5F0095;
    filter: brightness(0.5);
}

</style>

<script>
<?php if(!$especial): ?>
$(".folder-list .folder-list").hide();
<?php endif; ?>


$(".file-box").hide();
$(document).ready(function(){
    $(".folder-supracategoria").click(function(){
        $(this).siblings(".folder-list").slideToggle();
    })
    $(".folder-categoria").click(function(){
        $(".folder-categoria").removeClass("active");
        $(".folder-categoria").find("i").removeClass("fa-folder-open");
        $(".folder-categoria").find("i").addClass("fa-folder");
        var categoria = $(this).attr("data-categoria");
        $(this).addClass("active");
        $(this).find("i").removeClass("fa-folder");
        $(this).find("i").addClass("fa-folder-open");
        console.log("categoria click"+categoria)
        $(".file-box").fadeOut(80);
        $(".file-box").each(function(){
            if($(this).attr("data-categoria") == categoria){
                $(this).fadeIn(80);
            }
        })

    })


})


</script>
        

        <script>
        $(document).ready(function(){
            $('.file-box').each(function() {
                animationHover(this, 'pulse');
            });
        });
    </script>
