<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>SAL | </title>

    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">


    <link href="<?php echo base_url(); ?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/style.css?<?php echo time(); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/plugins/sweetalert/sweetalert.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/custom.css?<?php echo time(); ?>" rel="stylesheet">

    <!-- Mainly scripts -->
    <script src="<?php echo base_url(); ?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/metisMenu/jquery.metisMenu.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/sweetalert/sweetalert.min.js'); ?>"></script>
     <!-- jquery UI -->
    <script src="<?php echo base_url(); ?>assets/js/plugins/jquery-ui/jquery-ui.min.js"></script>

    <!-- Touch Punch - Touch Event Support for jQuery UI -->
    <script src="<?php echo base_url(); ?>assets/js/plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>

    <!-- Custom and plugin javascript -->
   
    <script src="<?php echo base_url('assets/js/plugins/pace/pace.min.js'); ?>"></script>

    <script src="<?php echo base_url('assets/js/plugins/chartJs/Chart.min.js'); ?>"></script>
    
    <!-- Flot -->
    <script src="<?php echo base_url('assets/js/plugins/flot/jquery.flot.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/flot/jquery.flot.tooltip.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/flot/jquery.flot.spline.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/flot/jquery.flot.resize.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/flot/jquery.flot.pie.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/flot/jquery.flot.symbol.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/flot/curvedLines.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/sparkline/jquery.sparkline.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/peity/jquery.peity.min.js'); ?>"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/iCheck/icheck.min.js"></script>

    <!-- Color picker -->
    <script src="<?php echo base_url();  ?>assets/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <link href="<?php echo base_url();  ?>assets/css/plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet">

</head>

<body>

        
            
