<?php $user = User_model::find($this->Base_model->get_user_id()); ?>
<nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav nav-custom metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                        
                            <img alt="image" width="50" class="img-circle" src="<?php echo $user->imagen? base_url()."assets/uploads/files/".$user->imagen : base_url()."assets/img/default male.png"; ?>" />
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" aria-extended="false" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?php echo $user->nombre; ?></strong>
                             </span> <span class="text-muted text-xs block"><?php echo implode(', ',$user->roles()->pluck('nombre')->toArray()); ?></span> </span> </a>

                    </div>
                          
                </li>
                <?php if(in_array("Administrador",$user->roles()->pluck('nombre')->toArray()) || in_array("Colegio",$user->roles()->pluck('nombre')->toArray()) ): ?>

                <li>
                    <a href="<?php echo base_url('archivo') ?>"><i class="fas fa-ruler-combined"></i> <span class="nav-label">Mecanizadas</span></a>
                </li>
                <?php endif; ?>
                <li>
                    <a href="<?php echo base_url('documento/index/informe') ?>"><i class="fas fa-file-alt"></i> <span class="nav-label">INFORMES DE GESTIÓN</span></a>
                </li>
                <li>
                    <a href="<?php echo base_url('documento/index/grilla') ?>"><i class="fas fa-file-alt"></i> <span class="nav-label">GRILLAS SALARIALES</span></a>
                </li>
                <li>
                    <a href="<?php echo base_url('documento') ?>"><i class="fas fa-file-alt"></i> <span class="nav-label">DOCUMENTOS</span></a>
                </li>
                



                <?php if(in_array("Administrador",$user->roles()->pluck('nombre')->toArray())): ?>
                <li>
                    <a href="<?php echo base_url('supracategoria') ?>"><i class="fas fa-file-alt"></i> <span class="nav-label">Categorías</span></a>
                </li>
                <li>
                    <a href="<?php echo base_url('categoria') ?>"><i class="fas fa-file-alt"></i> <span class="nav-label">Subcategorías</span></a>
                </li>
                <li>
                    <a href="<?php echo base_url('colegio') ?>"><i class="fas fa-graduation-cap"></i> <span class="nav-label">Colegios</span></a>
                </li>
                <li>
                    <a href="<?php echo base_url('usuario') ?>"><i class="fas fa-users"></i> <span class="nav-label">Usuarios</span></a>
                </li>
                <li>
                    <a href="<?php echo base_url('rol') ?>"><i class="fas fa-user"></i> <span class="nav-label">Roles</span></a>
                </li>
                <?php endif; ?>
                <li>
                
                    <a href="<?php echo base_url('login/logout') ?>"><i class="fas fa-sign-out-alt"></i> <span class="nav-label">Salir</span></a>
                </li>
            </ul>

        </div>
    </nav>

    <style>
        .nav-label{
            text-transform: uppercase;
            font-size: 12px;
        }
    </style>