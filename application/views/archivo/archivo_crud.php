<div class="gray-bg" id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <h1 class="titulo"><?php echo $titulo ?></h1>
            <hr>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content tabla-elementos">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                <!-- crud -->
                                        <?php $css_files = $table->css_files; ?>
                                        <?php $js_files = $table->js_files; ?>
                                        <?php $js_lib_files = $table->js_lib_files; ?>
                                        <?php $js_config_files = $table->js_config_files; ?>
                                        <?php $output = $table->output; ?>
                                        <!-- Para el CRUD -->
                                        <?php foreach($css_files as $file): ?>
                                       <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
                                        <?php endforeach; ?>

                                        <?php $flag=true;?>

                                        <?php foreach($js_files as $file): ?>
                                                <script src="<?php echo $file; ?>"></script>
                                        <?php endforeach; ?>


                                        <!-- Para el CRUD -->
                                        <?php echo $output?>
                                <!-- crud -->
                            <?php if(isset($extra)): ?>
                                <?php echo $extra; ?>
                            <?php endif; ?>
                            </div>
                        </div>
                        <a href="<?php echo base_url(); ?>archivo/upload_bulk" class="btn btn-primary">Subir en lote</a>
                    </div>
                </div>
            </div>
            <hr>
        </div>
    </div>
</div>

        


