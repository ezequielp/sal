  <!-- CodeMirror -->

<link href="<?php echo base_url(); ?>assets/css/plugins/dropzone/dropzone.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/plugins/dropzone/basic.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/js/plugins/dropzone/dropzone.js"></script>


<div class="gray-bg" id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <h1 class="titulo">Subir lote</h1>
            <hr>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content tabla-elementos">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">

                            <form action="<?php echo base_url(); ?>archivo/uploads" enctype="multipart/form-data" class="dropzone" id="myAwesomeDropzone">

                                <div class="form-group">
                                    <h5>Nombre</h5>
                                    <input class="form-control" type="text" name="nombre">
                                    <p class="help-block small">Nombre de la galeria.</p>
                                </div>

                                <div class="fallback">
                                    <input name="file" type="file" multiple />
                                </div>
                            </form>
                            
                            <div class="final-info">
                                    <label for="uploaded_files">Respuesta de subida de archivos</label>
                                    <input type="text" id="uploaded_files">
                            </div>
 
                            <?php if(isset($extra)): ?>
                                <?php echo $extra; ?>
                            <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
        </div>
    </div>
</div>

        

<script>
    Dropzone.options.myAwesomeDropzone = {
        autoProcessQueue: true,
        uploadMultiple: true,
        parallelUploads:10000,
        successmultiple:function(data,response){
                $("#uploaded_files").val(response);
        },
        init: function() {
        },
        complete: function(file){
        }
}; 


</script>

