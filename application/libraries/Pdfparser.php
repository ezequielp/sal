<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Smalot\PdfParser\Parser as Parser;

class Pdfparser {

        public function pdf2string($filename){
             
            // Parse pdf file and build necessary objects.
            $parser = new Parser();
            $pdf    = $parser->parseFile($filename);
             
            $text = $pdf->getText();
            return $text;

        }
}

