<?php
class Migration_Instalacion extends CI_Migration
{
	/**
	 * up
	 */
	public function up()
	{
		echo "database Iniciacion...\n";


                $dir = 'sql';
                $allFiles = scandir($dir); // Or any other directory
                $files = array_diff($allFiles, array('.', '..'));
                
                echo "importando desde {$dir}\n";
                
		$database_name = $this->db->database;
                $user_name = $this->db->username;
                $password = $this->db->password;
                
                foreach($files as $file) {
                    $file = "sql/".$file;
                    echo "importando datos...\n";
                    $commond = "mysql -u{$user_name} {$database_name}  < {$file}";
                    if($password){
                            $commond = "mysql -u{$user_name} -p{$password} {$database_name} < {$file}";
                    }
                    system($commond);
                    echo "\n".$commond.'done '.$file."! \n";
                    
                }
                
                


	}
	/**
	 * rollback
	 */
	public function down()
	{
		echo "database restore....\n";
		self::up();
		echo "Done. \n";
	}
}


