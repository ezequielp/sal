<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supracategoria extends CI_Controller {

	public function index($cuit = null)
	{
        if(!$this->Base_model->is_logged()){
            redirect('login');
        }
            $this->load->library('grocery_CRUD');

            $crud = new grocery_CRUD();
            
            $crud->set_theme('bootstrap');
            $crud->set_table('supracategoria');

            $user = User_model::find($this->Base_model->get_user_id());


            if(!in_array("Administrador",$user->roles()->pluck('nombre')->toArray())){
                $crud->unset_add();
                $crud->unset_edit();
                $crud->unset_delete();
                $crud->unset_read();  
            }
            else{

            }

            $crud->unset_fields('created_at','updated_at');
            
            $table = $crud->render();

            $output = array();

            $output['table'] = $table;
            $output['titulo'] = "Categorias";

            $this->output(Array("layout/menu","crud"),$output);
    }

 

    function output($vista = Array(), $data = null){
            
        $this->load->view('layout/header');
        if($vista){
            foreach ($vista as $item) {
                $this->load->view($item,$data);    
            }
        }
        $this->load->view('layout/footer');
    }
    
}
