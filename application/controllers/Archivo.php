<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Archivo extends CI_Controller {

	public function index($cuit = null)
	{
        if(!$this->Base_model->is_logged()){
            redirect('login');
        }
            $this->load->library('grocery_CRUD');

            $crud = new grocery_CRUD();
            
            $crud->set_theme('bootstrap');
            $crud->set_table('archivo');

            $user = User_model::find($this->Base_model->get_user_id());

            $crud->columns("nombre","codigo_colegio","codigo_liquidacion","nomenclatura","mes","año","orden","distrito","dipregep");
  
            $crud->callback_column('nombre',array($this,'_callback_archivo'));

            if(!in_array("Administrador",$user->roles()->pluck('nombre')->toArray())){
                foreach($user->colegios as $colegio){
                    $crud->or_where("codigo_colegio",$colegio->codigo);
                }
                $crud->unset_add();
                $crud->unset_edit();
                $crud->unset_delete();
                $crud->unset_read();
                
            }
            else{

            }

            $table = $crud->render();

            $output = array();

            $output['table'] = $table;
            $output['titulo'] = "Archivos";

            $this->output(Array("layout/menu","archivo/archivo_crud"),$output);
    }

    public function _callback_archivo($value,$row){
        return '<a target="_blank" href="'.base_url().'assets/uploads/liquidaciones/'.$value.'">Descargar</a>';
    }
    
    public function upload_bulk(){
        $output = Array();
        $this->output(Array("layout/menu","archivo/upload_bulk"),$output);
    }

    public function uploads()
    {
        $data = array();
        if (!empty($_FILES['file']['name'])) {
            
            $filesCount = count($_FILES['file']['name']);
            //Directory where files will be uploaded
            $uploadPath = 'assets/uploads/liquidaciones';
            $config['upload_path'] = $uploadPath;
            // Specifying the file formats that are supported.
            $config['allowed_types'] = 'jpg|png|pdf|doc|docx|xls|xlsx|ppt|pptx|txt|rtf|jpeg|zip';
            $this->load->library('upload', $config);
            $this->load->library('pdfparser');

            

            for ($i = 0; $i < $filesCount; $i++) {
                $full_name = str_replace(",","_",$_FILES['file']['name'][$i]);
                $_FILES['uploadFile']['name'] = date("Y-m-d-H-i-s").str_replace(",","_",$_FILES['file']['name'][$i]);
                $_FILES['uploadFile']['type'] = $_FILES['file']['type'][$i];
                $_FILES['uploadFile']['tmp_name'] = $_FILES['file']['tmp_name'][$i];
                $_FILES['uploadFile']['error'] = $_FILES['file']['error'][$i];
                $_FILES['uploadFile']['size'] = $_FILES['file']['size'][$i];

                
                $this->upload->initialize($config);
                if ($this->upload->do_upload('uploadFile')) {
                    $fileData = $this->upload->data();
                    $uploadData[$i]['file_name'] = $fileData['file_name'];

                    if($_FILES['uploadFile']['type'] == "application/zip"){
                        //Es un ZIP descomprimimos y hacemos todo el proceso
                        $zip = new ZipArchive;
                       
                        $res = $zip->open($uploadPath."/".$_FILES['uploadFile']['name']);
                        if ($res === TRUE) {
                            
                            $zip->extractTo($uploadPath);
                            
                            for( $j = 0; $j < $zip->numFiles; $j++ ){ 
                              $stat = $zip->statIndex( $j ); 
                              
                              rename(realpath($uploadPath)."/".$stat['name'],realpath($uploadPath)."/".date("Y-m-d-H-i-s").$stat['name']);

                              $archivo = new Archivo_model();
                    
                              $archivo->nombre = date("Y-m-d-H-i-s").$stat['name'];
                              $archivo->save();
                              
                              $partes_nombre = explode(".",$stat['name'] );
                              $archivo->distrito = $partes_nombre[2];
                              $archivo->codigo_colegio = substr($partes_nombre[3],0,4);
                              
                              $indices = Array();
                              preg_match('/N\/R PESOS  ([0-9]*)  (.*)/',$this->pdfparser->pdf2string($uploadPath."/".date("Y-m-d-H-i-s").$stat['name'] ),$indices);
          
                              //Armamos el acronimo de nomenclatura
                              if(preg_match_all('/\b(\w)/',strtoupper($indices[0]?? ""),$m)) {
                                  $acronimo = implode('',$m[1]?? "")." ".date("Y-m-d"); // $v is now SOQTU
                                }
                                
                              $archivo->codigo_liquidacion = $indices[1]?? 0;
                              $archivo->nomenclatura = $acronimo?? "";
                              $archivo->liquidacion = $indices[2]?? "";
                              $archivo->save();
                              $this->session->set_userdata('archivos_procesados',$j);
                            }
                            echo $j." Archivos descomprimidos";
                            
                        } else {
                            echo 'Error descomprimiendo';
                        }
                        
                    }
                    else{
                        
                        $archivo = new Archivo_model();
                        $archivo->nombre = $fileData['file_name'];
                        $archivo->save();
                        
                        $partes_nombre = explode(".",$full_name);
                        $archivo->distrito = $partes_nombre[2];
                        $archivo->codigo_colegio = substr($partes_nombre[3],0,4);
                        
                        $indices = Array();
                        preg_match('/N\/R PESOS  ([0-9]*)  (.*)/',$this->pdfparser->pdf2string($uploadPath."/".$fileData['file_name']),$indices);
                        
                        //Armamos el acronimo de nomenclatura
                        if(preg_match_all('/\b(\w)/',strtoupper($indices[0]?? ""),$m)) {
                            $acronimo = implode('',$m[1]?? "")." ".date("Y-m-d"); // $v is now SOQTU
                        }
                        
                        $archivo->codigo_liquidacion = $indices[1]?? 0;
                        $archivo->nomenclatura = $acronimo?? "";
                        $archivo->liquidacion = $indices[2]?? "";
                        $archivo->save();
                        $this->session->set_userdata('archivos_procesados',$i);
                        
                    }
                    
                }
                else{
                    echo $this->upload->display_errors('<p>', '</p>');
                }
                
            }
            if (!empty($uploadData)) {
                $list=array();
                foreach ($uploadData as $value) {
                    array_push($list, $value['file_name']);
                }
                echo count($list)." archivos subidos";
            }
            $this->upload->display_errors();
        }
    }

    function output($vista = Array(), $data = null){
            
        $this->load->view('layout/header');
        if($vista){
            foreach ($vista as $item) {
                $this->load->view($item,$data);    
            }
        }
        $this->load->view('layout/footer');
    }
    
}
