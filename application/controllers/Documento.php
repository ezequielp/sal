<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Documento extends CI_Controller {

	public function index($tipo = null)
	{
        if(!$this->Base_model->is_logged()){
            redirect('login');
        }
            $this->load->library('grocery_CRUD');

            $crud = new grocery_CRUD();
            $output = array();
            
            $crud->set_theme('bootstrap');
            $crud->set_table('documento');

            
            $user = User_model::find($this->Base_model->get_user_id());
            $crud->set_field_upload('nombre','assets/uploads/files/documentos');
            $crud->set_field_upload('imagen','assets/uploads/files');
            $crud->set_relation('categoria_id','categoria','nombre');
            
            $crud->field_type('icono','dropdown',Array( 'fas fa-chart-bar' => 'Grafico',
            'fas fa-file' => 'Hoja',
            'fas fa-film' => 'Media',
            'fas fa-music' => 'Audio',
        ));
        
            $output['supracategorias'] = Supracategoria_model::all();
            $output['especial'] = 0;

            if($tipo == "grilla"){
                $output['supracategorias'] = Supracategoria_model::where('id',1)->get();
                $output['especial'] = 1;
            }
            else if( $tipo == 'informe'){
                $output['supracategorias'] = Supracategoria_model::where('id',4)->get();
                $output['especial'] = 1;
            }
            else{
                $output['supracategorias'] = Supracategoria_model::where('id','!=',4)->
                where('id','!=',1)->get();
            }

            $output['tags'] = array_unique(Documento_model::all()->pluck('tag','id')->toArray());

            $output['user'] = $user;

            if(!in_array("Administrador",$user->roles()->pluck('nombre')->toArray())){
                $crud->unset_add();
                $crud->unset_edit();
                $crud->unset_delete();
                $crud->unset_read();  
            }
            else{

            }
            
            $crud->unset_fields('created_at','updated_at');

            $table = $crud->render();


            $output['table'] = $table;
            $output['titulo'] = "Documentos";

            $this->output(Array("layout/menu","documento/crud"),$output);
    }

 

    function output($vista = Array(), $data = null){
            
        $this->load->view('layout/header');
        if($vista){
            foreach ($vista as $item) {
                $this->load->view($item,$data);    
            }
        }
        $this->load->view('layout/footer');
    }
    
}
