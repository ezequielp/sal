<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index($cuit = null)
	{
        if(!$this->Base_model->is_logged()){
            redirect('login');
        }

            $output['titulo'] = "Archivos";

            $this->output(Array("layout/menu","home/home"),$output);
	}

    function output($vista = Array(), $data = null){
            
        $this->load->view('layout/header');
        if($vista){
            foreach ($vista as $item) {
                $this->load->view($item,$data);    
            }
        }
        $this->load->view('layout/footer');
    }
    
}
