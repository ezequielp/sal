<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

        public function index($cuit = null)
        {

            if($this->Base_model->get_user_id()){
                redirect('home');
            }

            $data['titulo'] = "Bienvenido a Codeingiter Eloquent";
            $this->output(Array("login/login"),$data);

        }
        
        public function ingresar(){
            if($this->input->post('username') && $this->input->post('password')){
                if($this->Base_model->login($this->input->post('username'),$this->input->post('password'))){
                    $this->session->set_flashdata('msg', 'Bienvenido');
                    redirect('documento');
                }
                else{
                    $this->session->set_flashdata('msg', 'Usuario o Contraseña Incorrecta');
                    redirect('login');
                }
                
            }
        }
        
        public function logout(){
            $this->Base_model->logout();
            redirect('login');
        }
        
        function output($vista = Array(), $data = null){
            
            $this->load->view('layout/header');
            if($vista){
                foreach ($vista as $item) {
                    $this->load->view($item,$data);    
                }
            }
            $this->load->view('layout/footer');
        }
}
