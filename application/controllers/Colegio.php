<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Colegio extends CI_Controller {

	public function index($cuit = null)
	{
        if(!$this->Base_model->is_logged()){
            redirect('login');
        }
            $this->load->library('grocery_CRUD');

            $crud = new grocery_CRUD();
            
            $crud->set_theme('bootstrap');
            $crud->set_table('colegio');

            $table = $crud->render();

            $output = array();

            $output['table'] = $table;
            $output['titulo'] = "Colegios";

            $this->output(Array("layout/menu","crud"),$output);
	}
    
    function output($vista = Array(), $data = null){
            
        $this->load->view('layout/header');
        if($vista){
            foreach ($vista as $item) {
                $this->load->view($item,$data);    
            }
        }
        $this->load->view('layout/footer');
    }
    
}
