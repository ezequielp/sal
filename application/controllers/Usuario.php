<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller {

	public function index($cuit = null)
	{
        if(!$this->Base_model->is_logged()){
            redirect('login');
        }
            $this->load->library('grocery_CRUD');

            $crud = new grocery_CRUD();
            
            $crud->set_theme('bootstrap');
            $crud->set_table('user');
            $crud->set_relation_n_n('Roles', 'user_rol', 'rol', 'user_id', 'rol_id', 'nombre');
            $crud->set_relation_n_n('Colegios', 'user_colegio', 'colegio', 'user_id', 'colegio_id', 'nombre');
            $crud->columns('username','imagen','nombre','Roles','Colegios');

            $crud->set_field_upload('imagen','assets/uploads/files');
			$crud->callback_edit_field('password',array($this,'set_password_input_to_empty'));
            $crud->callback_add_field('password',array($this,'set_password_input_to_empty'));
            $crud->callback_before_update(array($this,'encrypt_password_callback'));
            $crud->callback_before_insert(array($this,'encrypt_password_callback_insert'));

            $table = $crud->render();

            $output = array();

            $output['table'] = $table;
            $output['titulo'] = "Usuarios";

            $this->output(Array("layout/menu","crud"),$output);
	}

    function encrypt_password_callback($post_array, $primary_key) {

        //Encrypt password only if is not empty. Else don't change the password to an empty field
        if(!empty($post_array['password']))
        {
    
            $post_array['password'] = sha1($post_array['password']);
        }
        else
        {
        unset($post_array['password']);
        }
    
        return $post_array;
        }
        function encrypt_password_callback_insert($post_array) {
    
    
        //Encrypt password only if is not empty. Else don't change the password to an empty field
        if(!empty($post_array['password']))
        {
            $post_array['password'] = sha1($post_array['password']);
        }
        else
        {
        unset($post_array['password']);
        }
    
        return $post_array;
        }
    
        function set_password_input_to_empty() {
            return "<input type='password' name='password' value='' />";
        }
        
    
    function output($vista = Array(), $data = null){
            
        $this->load->view('layout/header');
        if($vista){
            foreach ($vista as $item) {
                $this->load->view($item,$data);    
            }
        }
        $this->load->view('layout/footer');
    }
    
}
